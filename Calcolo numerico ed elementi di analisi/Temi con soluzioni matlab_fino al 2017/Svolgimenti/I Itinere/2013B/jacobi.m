function [ x, k ] = jacobi( A, b, x0, tol, itermax)

if (prod(diag(A)) == 0)
    error('det(P_J)=0')
end

D_inv = diag(1./diag(A));
x = x0;
r = b - A*x0;
err = norm(r)/norm(b);
k = 0;

while (err > tol) && (k < itermax)
	k = k+1;
		
  	z = D_inv*r;
  	x = x + z;
    r = b - A*x;
    
  	err = norm(r)/norm(b);
end


return


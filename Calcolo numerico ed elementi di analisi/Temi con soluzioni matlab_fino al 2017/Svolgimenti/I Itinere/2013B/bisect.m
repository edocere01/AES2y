function [ x, k ] = bisect( a,b,f,tol1,tol2 )

x0 = (a+b)/2;
x = x0;
k = 0;

k_min = ceil( log2((b-a)/tol1) - 1 );

err = tol2 +1;

while (k < k_min) && (err > tol2)
    k = k+1;
    
    if f(x) == 0
       break 
    end
    
    if f(a)*f(x) < 0
        b = x;
    elseif f(x)*f(b) <0
        a = x;
    end
    
    x = (a+b)/2;
    
    err = abs(f(x));
        
end


end


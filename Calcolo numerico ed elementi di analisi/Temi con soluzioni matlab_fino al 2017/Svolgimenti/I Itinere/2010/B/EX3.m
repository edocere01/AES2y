clear all
close all

a = 0; b = pi/2;
f = @(x) sin(x);

I_vect = [];

I_ex = 1;

for M = 1:100
   
    I = trapcomp(a,b,f,M);
    
    I_vect = [I_vect; I];
    
end

e_vect = I_ex - I_vect;

H_vect = (b-a)./[1:M];

loglog(H_vect, e_vect)
grid on
hold on
plot(H_vect, H_vect.^2, '--k');
legend ('e_{t}^{c}I', 'H^{2}')



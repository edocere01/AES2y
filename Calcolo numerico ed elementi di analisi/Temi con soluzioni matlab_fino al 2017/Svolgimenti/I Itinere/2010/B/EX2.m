close all
clear all

set(0,'defaultTextInterpreter','latex')


%% EQUALLY SPACED NODES

f = @(x) tanh(x.^4);
a = -2; b = 2;

n_vect = [5 9 13 17];


E_n_matr = [];
e_n_vect = [];


for k = 1:length(n_vect)
    
   n = n_vect(k);
   x = linspace(-2, 2, n+1);
   y = f(x);
   
   p = polyfit(x,y,n);
   z = [a:0.001:b];
   
   pz = polyval(p,z);
   
   E_n = (f(z) - pz)';
   E_n_matr = [E_n_matr E_n];
   
   e_n = max(abs(E_n));
   e_n_vect = [e_n_vect; e_n];

end



e_n_vect

figure
plot(z, E_n_matr)
grid on
title('Interpolation-Error Function for \textbf{equally-spaced nodes} (\textit{Runge''s phenomenon} is clearly visible)')
legend('E_{5}f(x)', 'E_{9}f(x)', 'E_{13}f(x)', 'E_{17}f(x)')


%% CGL (Chebishev-Gauss-Lobatto) NODES

f = @(x) tanh(x.^4);
a = -2; b = 2;

n_vect = [5 9 13 17];


E_n_matr = [];
e_n_vect_CGL = [];


for k = 1:length(n_vect)
    
   n = n_vect(k);
   
   x = [];
   for i = 0:n
       x_i_hat = -cos(i*pi/n);
       x_i = (a+b)/2 + ((b-a)/2)*x_i_hat;
       x = [x x_i];
   end
   
   y = f(x);
   
   p = polyfit(x,y,n);
   z = [a:0.001:b];
   
   pz = polyval(p,z);
   
   E_n = (f(z) - pz)';
   E_n_matr = [E_n_matr E_n];
   
   e_n = max(abs(E_n));
   e_n_vect_CGL = [e_n_vect_CGL; e_n];
   
end



e_n_vect_CGL

figure
plot(z, E_n_matr)
grid on
hold on

title('Interpolation-Error Function for \textbf{CGL nodes} (\textit{Runge''s phenomenon} is avoided)')

plot(x, zeros(1,length(x)), 'o')

legend('E_{5}f(x)', 'E_{9}f(x)', 'E_{13}f(x)', 'E_{17}f(x)', '\{ x_{i}^{(CGL)} \}_{i=0}^{17}')




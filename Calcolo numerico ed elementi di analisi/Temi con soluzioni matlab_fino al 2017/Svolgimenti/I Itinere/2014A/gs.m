function [ xk, k ] = gs( A,b,x0,tol,itermax )

x = x0;
k = 0;

r = b-A*x;
err = norm(r)/norm(b);

T = tril(A);

while (err > tol) && (k < itermax)
    
    k = k+1;
    
    % solve Tz=r
    z = T\r;
    x = x + z;
    
    r = b-A*x;
    err = norm(r)/norm(b);
end

xk = x;

return


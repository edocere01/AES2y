close all
clear all

f = @(x) sin(pi*x);
a = 0; b = 4;
x = [a:0.001:b];
plot(x,f(x))
grid on



d4f = @(x) pi^4*sin(pi*x);

tol_vect = [1e-09 1e-07 1e-05 1e-03 1e-01];

I_vect = [];
M_vect = [];


for i = 1:length(tol_vect)
    
    tol = tol_vect(i);
    
    [ I, M ] = simpTolComp( a, b, f, d4f, tol );
    
    I_vect = [I_vect; I];
    M_vect = [M_vect; M];
    
end


I_vect
M_vect
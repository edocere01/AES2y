%% 2
a = [1 0 0]';
b = (1/24)*[9 19 -5 1]';
stabreg(a,b);
grid on



%% 3
t0 = 0;
tf = 20;

h = 0.1;
t_vect = [t0:h:tf];

f = @(t,y) -3*y + sin(t);
df = @(t,y) -3;
it_max = 1e+3;

y_ex = @(t) 0.1*(11*exp(-3*t) + 3*sin(t) - cos(t));
u0 = y_ex(h*[0 1 2])';


[~,u] = multistep (a, b, tf, t0, u0, h, f, df, tol, it_max);

plot(t_vect, u)

grid on
hold on
 
plot(t_vect, y_ex(t_vect))

legend('u(t)','y_{ex}')

%% 4

h_vect = 10.^-[0:3];

e_h_vect = [];

for k = 1:length(h_vect)
    
    h = h_vect(k);
    t_vect = [t0:h:tf];

    [~,u] = multistep (a, b, tf, t0, u0, h, f, df, tol, it_max);
    
    e_h = max(abs(u(end) - y_ex(tf)));
    e_h_vect = [e_h_vect e_h];
end

figure
loglog(h_vect, e_h_vect)
grid on
hold on





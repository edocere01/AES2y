%% 4.3
A = [2 -0.5 0 -0.5; 0 4 0 2; -0.5 0 6 0.5; 0 0 1 9];
B = [1/3 2/3 2 3; 1 0 -1 2; 0 0 -5/3 -2/3; 0 0 1 0];

n = length(diag(A));

tol = 1e-6;
itermax = 1e+4;
x0 = ones(n,1);
[ lambda_A, x_A, k_A ] = eigpower( A,tol,itermax,x0 )
disp('eigpower.m converge all''autovalore di modulo max per A')

[ lambda_B, x_B, k_B ] = eigpower( B,tol,itermax,x0 )
fprintf('eigpower.m diverge per A, poichè ci sono\ndue autovalori = 1 coincidenti e di modulo max\n')
function [t_h,u_h] = beuler(f,t0,tf,y0,h)

t_h = [t0:h:tf]';
u_h = [y0];

N_h = (tf-t0)/h;

itermaxFP = 100;
tolFP = 1e-5;

for n = 0: N_h-1
    
    t_n = t0 + n*h;
    t_np1 = t_n + h;
    
    u_n = u_h(end);
    
    phi = @(u_np1) u_n + h*f(t_np1,u_np1);
    [u_np1, ~] = fixedPt(u_n, phi, itermaxFP, tolFP);
    
    u_h = [u_h; u_np1];
end


%% EX.5

A = [1 1 0; 0 1 0; 0 1 .5];
B = [1 1 0; 1 2 0; 0 1 .5];

tol = 1e-6;
itermax = 1000;

[ lambdas, k ] = qrbasic( A, tol, itermax)
[ lambdas, k ] = qrbasic( B, tol, itermax)

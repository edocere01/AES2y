function I=pmedcomp(a,b,N,f)

h=abs(a-b)/N;%vettore di intervalli
x=(a+h/2:h:b);%vettore punti medi
I=h*sum(f(x));%integrale
function[t_h,u_h,iter_pf]=euleroindietro(f,t_max,y_0,h)

% vettore degli istanti in cui risolvo la ode
t0=0;
t_h=t0:h:t_max;

% inizializzo il vettore che conterra' la soluzione discreta
N_istanti=length(t_h);
u_h=zeros(1,N_istanti);

% ciclo iterativo che calcola u_(n+1)=u_n+h*f_(n+1) . 
% Ad ogni iterazione temporale devo eseguire delle sottoiterazioni
% di punto fisso per il calcolo di u_(n+1): 
%
% u_(n+1)^(k+1) = u_n + h * f_( t_(n+1) , u_(n+1)^k ). 
u_h(1)=y_0;

% parametri per le iterazioni di punto fisso
N_max=100;
toll=1e-5;
iter_pf=zeros(1,N_istanti);

for it=2:N_istanti
    
    % preparo le variabili per le sottoiterazioni
    u_old=u_h(it-1);
    t_pf=t_h(it);
      
    phi=@(u) u_old + h * f( t_pf, u );
    
    % sottoiterazioni
    [u_pf, it_pf] = ptofis2(u_old, phi, N_max, toll);
    u_h(it)=u_pf(end);
    
    % tengo traccia dei valori di it_pf per valutare la convergenza 
    % delle iterazioni di punto fisso
    iter_pf(it)=it_pf;
    
end
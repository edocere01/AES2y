function [lambda,x ,iter]=eigpower(A,tol,nmax,x0)

[n,m]=size(A);
if(n~=m)
    error('solo quadrate')
end
    
if nargin==1
    tol=1e-6;
    x0=ones(n,1);
    nmax=100;
end

iter=0;
y=x0/norm(x0);
lambda=y'*A*y;
err=tol*abs(lambda)+1;

while(err>tol*abs(lambda)&&iter<nmax)
    iter=iter+1;
    x=A*y;
    y=x/norm(x);
    newlamb=y'*A*y;
    err=abs(newlamb-lambda);
    lambda=newlamb;
end
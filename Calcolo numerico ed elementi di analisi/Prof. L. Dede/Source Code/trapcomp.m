function I = trapcomp(a,b,N,fun)

h=abs(a-b)/N; %vettore intervalli
x=[a:h:b];%vettore dei punti
I=h/2*(fun(x(1))+fun(x(N+1))+2*sum(fun(x(2:N))));%integrale
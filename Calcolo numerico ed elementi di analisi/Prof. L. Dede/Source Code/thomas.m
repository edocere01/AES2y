function [L,U,x]=thomas(A,b)

n=size(A,1);
alfa=zeros(n,1);
delta=zeros(n-1,1);

%inizializzo le quantita
c=diag(A,1);
e=diag(A,-1);
a=diag(A,0);
alfa(1)=a(1);
%calcolo gli alfa e i denta
for i=2:n
    delta(i-1)=e(i-1)/alfa(i-1);
    alfa(i)=a(i)-delta(i-1)*c(i-1);
end
%faccio le 2 matrici l ed u
L=diag(ones(n,1),0)+diag(delta,-1);
U=diag(alfa,0)+diag(c,1);
%creo il vettore y 
y=zeros(n,1);
y(1)=b(1);

for i=2:n
      y(i)=b(i)-delta(i-1)*y(i-1);
end

%creo il vettore x
x=zeros(n,1);
x(n)=y(n)/alfa(n);

for i=n-1:-1:1
      x(i)=(y(i)-c(i)*x(i+1))/alfa(i);
end
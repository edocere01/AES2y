function gershcircles_mod(A)
%   GERSHCIRCLES_MOD disegna i cerchi di Gershgorin 
%    
%   GERSHCIRCLES_MOD(A) disegna i cerchi di
%   Gershgorin associati alla matrice  A ed
%   alla sua trasposta. I cerchi sono tutti nella
%   stessa finestra per agevolare la loro interpretazione
n = size(A);
if n(1) ~= n(2)
    error('Solo matrici quadrate');
else
    n = n(1);
    circler = zeros(n,201);
    circlec = circler;
end
center = diag(A);
radiic = sum(abs(A-diag(center)));
radiir = sum(abs(A'-diag(center)));
one = ones(1,201);
cosisin = exp(i*[0:pi/100:2*pi]);
for k = 1:n
   circlec(k,:) = center(k)*one + radiic(k)*cosisin;
   circler(k,:) = center(k)*one + radiir(k)*cosisin;
end
for k = 1:n
   figure(1);
   plot(real(circler(k,:)),imag(circler(k,:)),'g','LineWidth', 2);
   hold on
   plot(real(circlec(k,:)),imag(circlec(k,:)),'k-');
   plot(real(center(k)),imag(center(k)),'rx','MarkerSize', 8);
end 
% title('Cerchi di Gershgorin')
xlabel('Re'); ylabel('Im');
legend('cerchi riga', 'cerchi colonna', 'centro cerchi','Location','NorthOutside' );
figure(1);
axis image;
hold off
return

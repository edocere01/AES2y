function [ lambda, x, k ]=eigpower( A, tol, itermax, x0 )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

y = x0/norm(x0);
lambda = y'*A*y;

k = 0;

err = tol + 1;

while (err > tol) && (k < itermax)
    
    k = k+1

    x = A*y;    
    y = x/norm(x);
    
    lambda_old = lambda;
    lambda = y'*A*y;
    
    err = abs(lambda - lambda_old)/abs(lambda);
    
end



end



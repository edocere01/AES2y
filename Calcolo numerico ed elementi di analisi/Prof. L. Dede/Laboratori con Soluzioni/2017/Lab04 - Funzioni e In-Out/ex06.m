close all

a = [sqrt(2)];
p = [2*a(1)];

b = [2];
q = [2*b(1)]

N = 10;

for n=2:N
   a_n = sqrt(2)*sqrt(1-sqrt(1-.25*(a(n-1))^2));
   a = [a a_n];
   
   b_n = a_n/sqrt(1-.25*(a(n-1))^2);
   b = [b b_n];


   p = [p (2^n)*a_n];
   q = [q (2^n)*b_n];

   
end

p
q

figure
grid on
hold on
plot([1:N], p, '.-r', 'MarkerSize', 20)
plot([1:N], q, '.-b', 'MarkerSize', 20)
plot([1:N], linspace(pi,pi,N), '--k', 'LineWidth', 2)

legend ('p', 'q', '\pi')


function [det]=det23(A)
% function [det]=det23(A)
% Laboratorio 4 Esercizio 1: scrivere una funzione che calcoli il
% determinante di matrici quadrate di ordine 2 e 3

[n,m]=size(A);
if n==m
    if n==2
        det=A(1,1)*A(2,2)-A(2,1)*A(1,2);
    elseif n==3
        det = A(1,1)*det23(A([2 3],[2 3])) - ...
              A(1,2)*det23(A([2 3],[1 3])) + ...
              A(1,3)*det23(A([2 3],[1 2]));
    else
        error('Solo matrici 2x2 o 3x3');
    end
else 
    error('Solo matrici quadrate');
end
return     
    

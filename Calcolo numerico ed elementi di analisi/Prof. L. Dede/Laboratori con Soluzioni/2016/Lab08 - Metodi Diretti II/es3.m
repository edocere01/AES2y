clear all

K=100;
Lungh=20;

iter=10;

T=zeros(4,iter);

for i=1:iter

      disp(strcat('iter ',num2str(i)))
      %aumento la dimensione
      n=200*i;

      %costruisco la matrice
      extradiag=ones(n-1,1);
      maindiag=-2*ones(n,1);
      A=diag(maindiag,0)+diag(extradiag,1)+diag(extradiag,-1);
      A=K*A;
      %costruisco il termine noto
      t_noto=zeros(n,1);
      %aggiungo delle forzanti esterne
      %t_noto=16*rand(n,1);
      t_noto(end)=t_noto(end)-K*Lungh;


      %risolvo con\, LU, thomas, Choleski e memorizzo nella matrice T

      t=tic;
      x_back=A\t_noto;
      T(1,i)=toc(t);

      t=tic;
      [L,U]=lu(A);
      x_lu =U\(L\t_noto);
      T(2,i)=toc(t);

      t=tic;
      [L,U,x_thom]=thomas(A,t_noto);
      T(3,i)=toc(t);

      A_chol=-A;
      t_chol=-t_noto;
      t=tic;
      H=chol(A_chol);
      x_lu =H\(H'\t_chol);
      T(4,i)=toc(t);

end

figure;
hold on;
plot(T(1,:),'-ob');
plot(T(2,:),'-or');
plot(T(3,:),'-ok');
plot(T(4,:),'-og');

legend('backslash','lu','thomas','chol')
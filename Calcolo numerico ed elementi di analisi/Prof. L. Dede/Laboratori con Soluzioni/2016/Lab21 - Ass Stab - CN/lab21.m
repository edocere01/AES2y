%% lab21

clear all
salva=1;


%%  es 1: problemi di stabilita'


hlambda=-2.1;

h=0.05;
lambda=hlambda/h; %lambda=-42

t_max=1;

t_plot=0:0.001:t_max;

y0=2;

y=@(t) y0*exp(lambda*t);


f=@(t,y) lambda * y;

[t_h,u_h]=eulero_avanti(f,t_max,y0,h);


% confronto il risultato con la soluzione esatta

plot(t_plot,y(t_plot),'k','LineWidth',2);

leg=legend('y0 e^{\lambda t}',2);

set(leg,'FontSize',16);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);


if (salva==1), saveas(gcf,'Sol_analitica.eps','epsc2'), end

legend off

hold on;

%questa e' la sol. calcolata

plot(t_h,u_h,'-o','MarkerSize',4);


leg=legend('soluzione analitica','soluzione calcolata con EA',2);

set(leg,'FontSize',16);

%set(lab,'Position',[0.0007 0.2])

if (salva==1), saveas(gcf,'EA_sbaglia.eps','epsc2'), end


%% rifaccio tutto con Eulero Implicito, che in teoria non ha problemi. In realta' la nostra implementazione introduce un vincolo
% per hlambda, a causa della implementazione come punto fisso.  


% risolvo con passo h, che non garantisce la convergenza di EA
[EI_t_h,EI_u_h,vett_it_pf]=eulero_indietro(f,t_max,y0,h);


figure
% confronto il risultato con la soluzione esatta

plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on;

plot(EI_t_h,EI_u_h,'-o','MarkerSize',4);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);

leg=legend('soluzione analitica','soluzione calcolata con EI',4);

set(leg,'FontSize',16);


if (salva==1), saveas(gcf,'EI_sbaglia.eps','epsc2'), end

% se il punto fisso non converge raggiungo ad ogni passo temporale il massimo numero di iterazioni possibili:

figure

plot(vett_it_pf,'-o','MarkerSize',4)




%% risolvo nuovamente con EA, stavolta prendo h in modo tale da garantire la convergenza del metodo. Non convergerà
% invece EI

% prima era h=0.05; adesso impongo

hlambda2=-1.2;
h2=hlambda2/lambda

[t_h2,u_h2]=eulero_avanti(f,t_max,y0,h2);


% confronto il risultato con la soluzione esatta

plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on;

%questa e' la sol. calcolata con h2

plot(t_h2,u_h2,'-o','MarkerSize',4);



leg=legend('soluzione analitica','soluzione di EA con h\lambda_2',1);

set(leg,'FontSize',16);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);
%set(lab,'Position',[0.0007 0.2])

if (salva==1), saveas(gcf,'EA_ok_1.eps','epsc2'), end





%% se infatti risolvo con EI con passo h2, che garantisce la convergenza di EA ma e' ancora esterno alla zona di convergenza del punto
% fisso, EI non funziona

[EI_t_h2,EI_u_h2]=eulero_indietro(f,t_max,y0,h2);

figure
% confronto il risultato con la soluzione esatta

plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on;

plot(EI_t_h2,EI_u_h2,'-o','MarkerSize',4);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);

leg=legend('soluzione analitica','soluzione calcolata con EI',2);

set(leg,'FontSize',16);


if (salva==1), saveas(gcf,'EI_sbaglia2.eps','epsc2'), end


%% infine utilizzo un passo h3  

hlambda3=-0.7;
h3=hlambda3/lambda

[t_h3,u_h3]=eulero_avanti(f,t_max,y0,h3);


% confronto il risultato con la soluzione esatta

plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on;


% questa e' la sol. calcolata con h3

plot(t_h3,u_h3,'-or','MarkerSize',4);


leg=legend('soluzione analitica','soluzione di EA con h\lambda_3',1);

set(leg,'FontSize',16);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);
%set(lab,'Position',[0.0007 0.2])

if (salva==1), saveas(gcf,'EA_ok_2.eps','epsc2'), end


%% infine risolvo con passo h3, che garantisce la convergenza di EI
[EI_t_h3,EI_u_h3]=eulero_indietro(f,t_max,y0,h3);


figure
% confronto il risultato con la soluzione esatta

plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on;

plot(EI_t_h3,EI_u_h3,'-o','MarkerSize',4);

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);

leg=legend('soluzione analitica','soluzione calcolata con EI',2);

set(leg,'FontSize',16);


if (salva==1), saveas(gcf,'EI_ok.eps','epsc2'), end





%% esercizio 2
 
clear all
close all
salva=1;

y0=2;
lambda=-42;

tmax=1;

t_plot=0:0.01:tmax;

y=@(x) y0*exp(lambda*x);


%% risolvo con Crank Nicolson

% questo e' il termine di destra della EDO

f= @(t,y) lambda*y;

% la sintassi della chiamata e' la stessa di prima

h=0.02;

[CN_t_h,CN_u_h,iter_pf]=Crank_Nicolson(f,tmax,y0,h);


% confronto il risultato con la soluzione esatta

plot(t_plot,y(t_plot),'k','LineWidth',2);

hold on;

plot(CN_t_h,CN_u_h,'or','MarkerSize',4);

%title('soluzione approssimata con Crank-Nicolson','FontSize',20)

xlabel('t','FontSize',16);
ylabel('y(t)','FontSize',16,'Rotation',0);

leg=legend('soluzione analitica','soluzione calcolata con CN, h=0.02',2);

set(leg,'FontSize',16)

if (salva==1), saveas(gcf,'CN.eps','epsc2'), end


%title('sottoiterazioni di punto fisso','FontSize',16)

figure

plot(iter_pf,'o:','MarkerSize',4)

xlabel('istante','FontSize',16);
ylabel('iterazioni di punto fisso','FontSize',16);


%title('sottoiterazioni di punto fisso','FontSize',16)

if (salva==1), saveas(gcf,'iterazioni_pf_CN.eps','epsc2'), end

%% confronto con un passo piu' fine
% 
% % raffino h
% 
% h=0.01;
% 
% [CN_t_h_fine,CN_u_h_fine]=Crank_Nicolson(f,tmax,y0,h);
% 
% 
% plot(t_plot,y(t_plot),'k','LineWidth',2);
% 
% hold on
% 
% plot(CN_t_h,CN_u_h,'o','MarkerSize',4);
% 
% plot(CN_t_h_fine,CN_u_h_fine,'or','MarkerSize',4);
% 
% %title('confronto diversi passi h','FontSize',16)
% 
% leg=legend('soluzione analitica','CN con h=0.05','CN con h=0.01',2);
% 
% xlabel('t','FontSize',16);
% ylabel('y(t)','FontSize',16,'Rotation',0);
% 
% 
% set(leg,'FontSize',16)
% 
% if (salva==1), saveas(gcf,'CN_confronto_h.eps','epsc2'), end


%% grafici errore

%passi=0.05:-0.01:0.01;
N=5;
dimezz=2.^(1:N);
passi=0.04./dimezz;


for it=1:N
      [EI_t_h,EI_u_h]=eulero_indietro( f, tmax, y0, passi(it) );
      [CN_t_h,CN_u_h]=Crank_Nicolson( f, tmax, y0, passi(it) );
      y_h=y( 0 : passi(it) : tmax  );
      errore_CN(it)=max( abs (y_h-CN_u_h) );
      errore_EI(it)=max( abs (y_h-EI_u_h) );
end


% figure;
% 
% plot( 0:passi(it):tmax, y(0:passi(it):tmax), 'k')
% hold on
% plot(EA_t_h,EA_u_h,'-ob');
% plot(EI_t_h,EI_u_h,'-or');



%figure;

% questo servira' dopo, e' un trucco per visualizzare meglio l'errore, separando le linee che indicano la pendenza della convrgenza dai
% risultati
%m=( errore_EA(1)-errore_EA(end) ) / (passi(1) -passi(end));

% plot(passi,errore_EA,'-ob','LineWidth',2);
% hold on;
% plot(passi,errore_EI,'-or','LineWidth',2);
% plot(passi,m*passi,'k')
%plot(passi,(m*passi).^2,'k:')


% figure;
% 
% semilogy(passi,errore_EA,'-ob','LineWidth',2);
% hold on
% semilogy(passi,errore_EI,'-or','LineWidth',2);
% plot(passi,passi,'k')
% plot(passi,passi.^2,'k:')

figure;

loglog(passi,errore_CN,'-ob','LineWidth',2);
hold on
loglog(passi,errore_EI,'-or','LineWidth',2);
plot(passi,passi,'k')
plot(passi,(passi).^2,'k:')

xlabel('h','FontSize',16);
lab=ylabel('err(h)','FontSize',16,'Rotation',0);
set(lab,'Position',[0.0007 0.2])

leg=legend('errore CN','errore EI','ordine 1','ordine 2',1);

set(leg,'FontSize',16)


if (salva==1), saveas(gcf,'andamento_errori_CN.eps','epsc2'), end


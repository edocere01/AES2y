clc
clear 
close all
format short
%% <DATI>
Vb = 65 * 1e-3; %metri cubi
Vc = 130 *1e-3; 
Pa = 0.1 * 1e6; %PASCAL
Pb = 0.2 * 1e6;
Pc = Pb;
lambda = 0.04;
l12 = 2; %metri
l34 = 10;
l45 = 2;
l46 = 8;
D = 0.01; %metri
S1 = 0.004; % presente nel ramo 45
S2 = 0.009; % presente nel ramo 46
ro = 850; %kg/m^3

Q34 = 24 * (1e-3 /60 ); %m^3/s è uguale a Q12 perchè la pompa preleva una porta e aumenta la pressione, non aumenta la portata!
P3 = 0.5 * 1e6 ; %PASCAL

%% COSTANTI K

%concentrate
k12 = lambda * l12 / D;
k34 = lambda * l34 / D;
k45 = lambda * l45 / D;
k46 = lambda * l46 / D;

%strozzature
ks1 = ks(S1,D);
ks2 = ks(S2,D);

K12 = K(k12+0.5,ro,D); % +0.5 imbocco
K34 = K(k34,ro,D);
K45 = K(k45+ks1+1,ro,D); % +1 per sbocco
K46 = K(k46+ks2+1,ro,D);

%% CASO Q34 NOTA 
% b = 5, c = 6, a = 1
Keq_45_Q = 1/((1/sqrt(K45))+(1/sqrt(K46)))^2; %Keq tra 4 e b parallelo
P4_Q = Pb + Keq_45_Q * Q34^2;
P3_Q = P4_Q + K34 * Q34^2;
P2_Q = Pa - K12 * Q34^2; % Q34 è uguale a Q12 perchè la pompa preleva una porta e aumenta la pressione, non aumenta la portata!
Q45_Q = sqrt((P4_Q - Pb)/K45);
Q46_Q = sqrt((P4_Q - Pb)/K46);

%rubinetto chiuso
P4_r_Q = Pb + K46 * Q34^2;
P3_r_Q = P4_r_Q + K34 * Q34^2;
% per veocità basta usare Q = v*A;

%% CASO P3
% b = 5, c = 6, a = 1
Keq_35_p = K34 + 1/((1/sqrt(K45))+(1/sqrt(K46)))^2; %Keq tra 3 e 5.
Q35_P = sqrt((P3-Pb)/Keq_35_p);
P4_P = P3 - Q35_P^2 * K34;
Q45_P = sqrt((P4_P - Pb)/K45);
Q46_P = sqrt((P4_P - Pb)/K46);
P2_P = Pa - K12 * Q35_P^2;

%rubinetto chiuso
Q36_r_P = sqrt((P3-Pb)/(K34+K46)); % Keq = K34 + K45, serie

%% CALCOLO TEMPISTICHE.
%Q34
tb_Q = Vb / Q45_Q; %seconid. A questo tempo chiudo il rubinetto
Vc_riempito_Q = Q46_Q * tb_Q; %metri cubi. A questo punto so che il rubinetto
% è stato chiuso quindi tutta la portata affluisce a Vc rimanente,
tc_Q = tb_Q + (Vc - Vc_riempito_Q)/Q34;

%P3
tb_P = Vb / Q45_P; %seconid. A questo tempo chiudo il rubinetto
Vc_riempito_P = Q46_P * tb_P; %metri cubi. A questo punto so che il rubinetto
% è stato chiuso quindi tutta la portata affluisce a Vc rimanente,
tc_P = tb_P + (Vc - Vc_riempito_P)/Q36_r_P;

%% STAMPA A VIDEO
%Q34
fprintf('***Q34 NOTA***\nle PORTATE [m^3/s] sono: \nQ12 = %g, \nQ34 = %g, \nQ45 = %g, \nQ46 = %g\n',Q34,Q34,Q45_Q,Q46_Q);
fprintf('le PRESSIONI [Pa] sono: \nP1 = %g, \nP2 = %g, \nP3 = %g, \nP4 = %g, \nP5 = P6 = %g\n',Pa,P2_Q,P3_Q,P4_Q,Pb);
fprintf('RUBINETTO CHIUSO: P3 = %g, P4 = %g\n',P3_r_Q, P4_r_Q);
fprintf('TEMPI [s]: tb = %g, tc =%g\n\n',tb_Q,tc_Q);
%P3
fprintf('***P3 NOTA***\nle PORTATE [m^3/s] sono: \nQ12 = %g, \nQ34 = %g, \nQ45 = %g, \nQ46 = %g\n',Q35_P,Q35_P,Q45_P,Q46_P);
fprintf('le PRESSIONI [Pa] sono: \nP1 = %g, \nP2 = %g, \nP3 = %g, \nP4 = %g, \nP5 = P6 = %g\n',Pa,P2_P,P3,P4_P,Pb);
fprintf('RUBINETTO CHIUSO: Q36 = %g\n',Q36_r_P);
fprintf('TEMPI [s]: tb = %g, tc =%g\n\n',tb_P,tc_P);


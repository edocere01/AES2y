function [k] = ks(Ds,Dt)
%KS restituisce il coeff di perdita concentrata nelle strozzature
%e rubinetto dati i diametri Ds e Dt 
As = pi * (Ds/2)^2;
At = pi * (Dt/2)^2;
k = ((1+0.707*((1-(As/At))^(1/2))-(As/At))^2) *(At/As)^2 ;
end


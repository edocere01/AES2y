clc
clear
close all
format short
%% PERIODO ECLISSI 30minuti
% I = 7; Corrente richiesta alle utenze
% consumo potenza istante per istante
p_utenze = @(t) 2/3*t + 15; % t in minuti 

% consumo totale durante l'eclissi
Energia_min = integral(p_utenze,0,30); % integrando ottengo Watt * min  (ENERGIA) (e non WATT (potenza) ;STUPIDO LE BASI)
Energia_h = Energia_min / 60; % Watt * h  


% Distributo nd = 98% **P_d prima del ditributore**
n_d = 98/100;
Energia_d = Energia_h /n_d;

% Regolatore di carica BDR; nBDR=95% (P_BDR) prima del bdr
n_BDR = 95/100;
Energia_BDR = Energia_d / n_BDR; %[Wh]

%% BATTERIA A  max corrente erogabile 9A
max_sa = 60/100; %massima scarica 60%
Esp_a = 65; %ENERGIA SPECIFICA Wh/kg

Energia_a = Energia_BDR/max_sa; % capacità batteria(per garantire 60% scarica)
massa_a = Energia_a / Esp_a; %massa in kg

fprintf('BATTERIA A');
fprintf('\nCapacita'': %f[Wh]',Energia_a);
fprintf('\nMassa: %f[kg]\n',massa_a);

%% BATTERIA B  max corrente erogabile 5A
%uso due batterie in parallelo che erogano 3.5A ciascuna
max_sb = 80/100; %%massima scarica 80%
Esp_b = 170; %ENERGIA SPECIFICA Wh/kg

Energia_b = Energia_BDR/max_sb;  % capacità batteria(per garantire 80% scarica)
massa_b = Energia_b / Esp_b;

fprintf('\nBATTERIA B');
fprintf('\nCapacita'': %f[Wh]',Energia_b);
fprintf('\nMassa Totale: %f[kg]',massa_b);
fprintf('\nMassa una batteria: %f[kg]',massa_b);
fprintf('\nDue batterie necessarie, Mtot: %f\n',2*massa_b);
%% DIMESIONAMENTO PANNELLI SOLARI.
n_pannello = 20/100; % rendimetno di conversione pannello
n_BCR = 95/100;
Intensita_sole = 1350;%[Watt/m^2] intensità ricevuta dal sole
ts = 70; %[min] tempo al sole

p_utenzes = 75; %[Watt]
p_BCR = p_utenzes / n_d ;
p_u = p_BCR/n_BCR; %[Watt]  potenza da fornire per soddisfare le utenze
Intensita_pannello = Intensita_sole * n_pannello; %[Watt/m^2]
%% BATTERIE:
%nella la potenza che va ricaricata; non si deve tenere conto della
%capacità in più;
%quindi è solo quella consumata dalle utenze!!! ovvero p_BDR
p_BDR = Energia_BDR * 60/ts; %converto l'Energia_BDR in Wmin e poi in Potenza

Sup = (p_u + p_BDR)/Intensita_pannello;
fprintf('\nSuperficie Pannello: %f[m^2]\n',Sup);

%il pannello solare è della stessa dimensione in entrambi i casi dato che

figure
plot(0,0:15,'b',0:2.5:30,p_utenze(0:2.5:30),'-*b',30,0:35,'*-b',30:5:100,0,'-b*')
hold on
plot(100*ones(16,1),0:15,'-b',100:2.5:130,p_utenze(0:2.5:30),'-b*',130,0:35,'-b*',130:5:200,0,'-b*')

%% CICLO CARICA SCARICA BATTERIA

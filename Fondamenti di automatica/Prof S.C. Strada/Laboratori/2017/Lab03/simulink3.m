s = tf('s');
L = 100/((1+s)^2*(1+0.01*s));

L_zpk = zpk(L) % me la porta in forma canonica
L_minreal = minreal(L) % mi fa le cancellazioni zero-polo (lecite)


%
figure
subplot(1,3,1)
bode(L)
grid on

subplot(1,3,2)
nyquist(L)
grid on

subplot(1,3,3)
step(L)
grid on

%%
[Gm, phi_m, omega_g, omega_c] = margin(L)

%Prestazioni STATICHE
e_inf = dcgain(1/(1+L))

%Smorzamento
xi = phi_m/100;
omega_n = omega_c;

%tempo assestamento
t_a = 4.6/(xi*omega_n)







